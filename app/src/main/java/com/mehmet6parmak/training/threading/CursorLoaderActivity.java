package com.mehmet6parmak.training.threading;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class CursorLoaderActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    CursorAdapterImpl adapter;
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);

        list = (ListView) findViewById(R.id.list);


        adapter = new CursorAdapterImpl(this, null, true);
        list.setAdapter(adapter);

        getSupportLoaderManager().initLoader(1, null, this);
    }

    public void updateText(View view) {

    }

    public void cancelUpdate(View view) {

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new ContactsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }


    static class ContactsLoader extends CursorLoader {

        private static final String[] PROJECTION = new String [] {
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME
        };

        public ContactsLoader(Context context) {
            super(context);
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = getContext().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, PROJECTION, null, null, ContactsContract.Contacts.DISPLAY_NAME);
            return cursor;
        }
    }

    public class CursorAdapterImpl extends CursorAdapter {
        public CursorAdapterImpl(Context context, Cursor c, boolean autoRequery) {
            super(context, c, autoRequery);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(CursorLoaderActivity.this);
            return inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            TextView txt = (TextView) view.findViewById(android.R.id.text1);
            txt.setText(cursor.getString(1));
        }
    }
}
