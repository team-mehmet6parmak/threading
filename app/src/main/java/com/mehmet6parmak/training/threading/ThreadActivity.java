package com.mehmet6parmak.training.threading;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class ThreadActivity extends AppCompatActivity {

    private static final String TAG = ThreadActivity.class.getSimpleName();

    TextView txt;

    public TextView getTxt() {
        return txt;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);

        txt = (TextView) findViewById(R.id.txt);
    }

    public void updateText(View view) {
        txt.setText("I am being updated...");

        //this anonymous inner class is holding a references to Activity! which will leak if the long running operation lasts after activity.
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String text = "I am up to date";

                    Thread.sleep(100000); //long running task.
                    txt.setText(text);
                    //updateUI(text);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        //new Thread(new LongRunningTask(this)).start();
    }

    public void updateUI(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txt.setText(text);
            }
        });

        //runOnUiThread(new UpdateTextView(txt, text));
    }

    //static inner classes does not hold reference to parent class. We would not leak activity if we us the following class.
    static class LongRunningTask implements Runnable {

        private WeakReference<ThreadActivity> activity;

        public LongRunningTask(ThreadActivity activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void run() {
            try {
                Thread.sleep(5000);
                if (activity.get() != null) {
                    Log.d(TAG, "Activity is not null, updating textview...");
                    activity.get().runOnUiThread(new UpdateTextView(activity.get().getTxt(), "I am up to date"));
                } else {
                    Log.d(TAG, "Activity is null, unable to update textview...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //static classes does not hold implicit references to parent object.
    static class UpdateTextView implements Runnable {

        private WeakReference<TextView> txt;
        private String text;

        public UpdateTextView(TextView txt, String text) {
            this.txt = new WeakReference<>(txt);
            this.text = text;
        }

        @Override
        public void run() {
            if (txt.get() != null) {
                Log.d(TAG, "setting text on TextView");
                txt.get().setText(text);
            } else {
                Log.d(TAG, "TextView is null, bypassing set operation");
            }
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }
}
