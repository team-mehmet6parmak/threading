package com.mehmet6parmak.training.threading;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class AsyncTaskLoaderActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    static int counter = 0;
    private static final String TAG = "AsyncTaskLoaderActivity";

    TextView txt;

    private static final int LOADER_UPDATE_TEXT_ID = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task_loader);

        txt = (TextView) findViewById(R.id.txt);
    }

    public void updateText(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("temp", counter++);

        getSupportLoaderManager().initLoader(LOADER_UPDATE_TEXT_ID, bundle, AsyncTaskLoaderActivity.this);
    }

    public void cancelUpdate(View view) {
        Loader<String> loader = getSupportLoaderManager().getLoader(LOADER_UPDATE_TEXT_ID);
        loader.cancelLoad();
    }

    public void updateTextFromScratch(View view) {
        getSupportLoaderManager().restartLoader(LOADER_UPDATE_TEXT_ID, null, AsyncTaskLoaderActivity.this);
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader: ");
        return new UpdateTextLoader(AsyncTaskLoaderActivity.this);
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        Log.d(TAG, "onLoadFinished: ");
        txt.setText(data);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        Log.d(TAG, "onLoaderReset: ");
        txt.setText("");
    }

    static class UpdateTextLoader extends AsyncTaskLoader<String> {
        private static final String TAG = "UpdateTextLoader";
        private String data = null;

        public UpdateTextLoader(Context context) {
            super(context);
            Log.d(TAG, "UpdateTextLoader: ctr");
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            if (data == null) {
                forceLoad();
            } else {
                deliverResult(data);
            }
        }

        @Override
        public String loadInBackground() {
            try {
                Log.d(TAG, "loadInBackground: started loading");
                Thread.sleep(10000);
                data = "I am up to date";
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return data;
        }
    }
}
