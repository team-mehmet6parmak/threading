package com.mehmet6parmak.training.threading;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onJavaThreadsClicked(View view) {
        startActivity(new Intent(this, ThreadActivity.class));
    }

    public void onAsyncTaskClicked(View view) {
        startActivity(new Intent(this, AsyncTaskActivity.class));
    }

    public void onLoaderClicked(View view) {
        startActivity(new Intent(this, CursorLoaderActivity.class));
    }

    public void onAsyncTaskLoaderClicked(View view) {
        startActivity(new Intent(this, AsyncTaskLoaderActivity.class));
    }
}
