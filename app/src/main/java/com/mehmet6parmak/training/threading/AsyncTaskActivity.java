package com.mehmet6parmak.training.threading;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class AsyncTaskActivity extends AppCompatActivity {

    TextView txt;
    UpdateTextTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);

        txt = (TextView) findViewById(R.id.txt);
    }

    public void updateText(View view) {
        task = new UpdateTextTask();
        task.execute();

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void cancelUpdate(View view) {
        if (task != null) {
            task.cancel(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        task.cancel(true);
    }

    /*

         Cons
         - Leaking activity,
         - Not following activity lifecycle, not aware of configuration changes...

         Pros
         - Using ThreadPool in background. Managing threads wisely.
         - Posting onPostExecute, onPreExecute, onProgressUpdate into UI thread automatically.


         */
    class UpdateTextTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            txt.setText("updating text...");
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                Thread.sleep(10000);

                // many of java apis are not interruptable, don't think you can successfully abort a running thread.
                // a way to cancel an ongoing operation is to check periodically the canceled flag.

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "I am up to date";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            txt.setText(s);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            txt.setText("canceled");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }
}
